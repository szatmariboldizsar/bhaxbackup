#include <curses.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{
	WINDOW *ablak;
	ablak=initscr();

	int xj = 0, xk = 0, yj = 0, yk = 0;
	int dx, dy;
	int m, sz;

	getmaxyx(ablak, m, sz);

	dx = sz*2;
	dy = m*2;

	while(true)
	{
		refresh();
		usleep(100000);
		clear();
		xj = (xj - 1) % dx;
		xk = (xk + 1) % dx;
		yj = (yj - 1) % dy;
		yk = (yk + 1) % dy;
		mvprintw(abs((yj + (dy - yk)) / 2), abs((xj + (dx - xk)) / 2), "O");
	}
}
